import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header>
        <h1>Form Pembelian Buah</h1>
      </header>

      <section>
        <table>
          <tr>
            <th>Nama Pelanggan</th>
            <td>
              <input type="text"></input>
            </td>
          </tr>

          <tr>
            <th className="alignBtm">Daftar Item</th>
            <td>
              <input type="checkbox" name="namaBuah"></input>Semangka<br></br>
              <input type="checkbox" name="namaBuah"></input>Jeruk<br></br>
              <input type="checkbox" name="namaBuah"></input>Nanas<br></br>
              <input type="checkbox" name="namaBuah"></input>Salak<br></br>
              <input type="checkbox" name="namaBuah"></input>Anggur<br></br>
            </td>
          </tr>
        </table>
        <button type="submit" className="btn">Kirim</button>
      </section>
    </div>
  );
}

export default App;

// Soal 1
console.log("==========Soal No.1==========");

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];

var objectDaftarPeserta = {
    nama : arrayDaftarPeserta[0],
    "Jenis Kelamin" : arrayDaftarPeserta[1],
    Hobi : arrayDaftarPeserta[2],
    "Tahun Lahir" : arrayDaftarPeserta[3]
}

console.log(objectDaftarPeserta);

// Soal 2
console.log("==========Soal No.2==========");

var Buah = [
    {nama : "strawberry", warna : "merah", "ada bijinya" : "tidak", harga : 9000},
    {nama : "jeruk", warna : "oranye", "ada bijinya" : "ada", harga : 8000},
    {nama : "semangka", warna : "hijau & merah", "ada bijinya" : "ada", harga : 10000},
    {nama : "pisang", warna : "kuning", "ada bijinya" : "tidak", harga : 5000},
];

console.log(Buah[0]);

// Soal 3
console.log("==========Soal No.3==========");

var dataFilm = [];

function tambahData(nama, durasi, genre, tahun) {
    var tambah = {
        "nama": nama,
        "durasi": durasi,
        "genre": genre,
        "tahun": tahun
    }
    return tambah;
}

dataFilm.push(tambahData("Black Widow", "2 jam", "action", "2020"));
console.log(dataFilm);

// Soal 4
console.log("==========Soal No.4==========");
// release 0
console.log("==========release 0==========");
class Animal {
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// release 1
console.log("==========release 1==========");
class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    }
    
    yell() {
        return console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    
    jump() {
        return console.log("hop hop");
    }
}
 
var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop" 

// Soal 5
console.log("==========Soal No.5==========");
class Clock {
    constructor({ template }) {
        var timer;
        
        function render() {
            var date = new Date();
        
            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;
        
            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;
        
            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;
        
            var output = template
              .replace('h', hours)
              .replace('m', mins)
              .replace('s', secs);
        
            console.log(output);
          }
        
          this.stop = function() {
            clearInterval(timer);
          };
        
          this.start = function() {
            render();
            timer = setInterval(render, 1000);
          }; 
    }

}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
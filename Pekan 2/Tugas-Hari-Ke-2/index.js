var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];
 
function lanjutBaca(waktu, buku) {
    readBooks(waktu, books[buku], function(sisaWaktu) {
        if(sisaWaktu > 0) {
            if(buku + 1 < books.length) {
                lanjutBaca(sisaWaktu, buku + 1);
            }
        }
    });
}

lanjutBaca(10000, 0);
// No 1.
console.log("==========Soal No.1==========")
function halo() {
    let greet = "Halo Sanbers!";
    return greet;
}

console.log(halo());

// No 2.
console.log("==========Soal No.2==========")
function kalikan(num1, num2) {
    return num1 * num2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// No 3.
console.log("==========Soal No.3==========")
function introduce(name, age, address, hobby) {
    let intro = `Nama saya ${name}, umur saya ${age}, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}`;
    return intro;
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
console.log("================================")

// Soal 1
console.log("Soal 1")

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama + " " + kataKedua.charAt(0).toUpperCase() + kataKedua.substr(1) + " " + kataKetiga + " " + kataKeempat.toUpperCase());

console.log("================================")

// Soal 2
console.log("Soal 2")

var nomorPertama = "1";
var nomorKedua = "2";
var nomorKetiga = "4";
var nomorKeempat = "5";

var nomor1 = Number(nomorPertama);
var nomor2 = Number(nomorKedua);
var nomor3 = Number(nomorKetiga);
var nomor4 = Number(nomorKeempat);

var jumlah = nomor1 + nomor2 + nomor3 +nomor4;
console.log(jumlah);

console.log("================================")

// Soal 3
console.log("Soal 3")

var kalimat = 'wah javascript itu keren sekali'; 

var kataKe1 = kalimat.substring(0, 3); 
var kataKe2 = kalimat.substring(4, 14);  
var kataKe3 = kalimat.substring(15, 18);  
var kataKe4 = kalimat.substring(19, 24);  
var kataKe5 = kalimat.substring(25);  

console.log('Kata Pertama: ' + kataKe1); 
console.log('Kata Kedua: ' + kataKe2); 
console.log('Kata Ketiga: ' + kataKe3); 
console.log('Kata Keempat: ' + kataKe4); 
console.log('Kata Kelima: ' + kataKe5);

console.log("================================")

// Soal 4
console.log("Soal 4")

var nilai = 75;

if (nilai >= 80) {
    console.log("Index nilai anda adalah A");
} else if (nilai >= 70 && nilai < 80) {
    console.log("Index nilai anda adalah B");
} else if (nilai >= 60 && nilai < 70) {
    console.log("Index nilai anda adalah C");
} else if (nilai >= 50 && nilai < 60) {
    console.log("Index nilai anda adalah D");
} else {
    console.log("Index nilai anda adalah E");
}

console.log("================================")

// Soal 5
console.log("Soal 5")

var hari = 2;
var bulan = 4;
var tahun = 1991;

switch (bulan) {
    case 1 : { console.log(hari + " Januari " + tahun); break;}
    case 2 : { console.log(hari + " Februari " + tahun); break;}
    case 3 : { console.log(hari + " Maret " + tahun); break;}
    case 4 : { console.log(hari + " April " + tahun); break;}
    case 5 : { console.log(hari + " Mei " + tahun); break;}
    case 6 : { console.log(hari + " Juni " + tahun); break;}
    case 7 : { console.log(hari + " Juli " + tahun); break;}
    case 8 : { console.log(hari + " Agustus " + tahun); break;}
    case 9 : { console.log(hari + " September" + tahun); break;}
    case 10 : { console.log(hari + " Oktober " + tahun); break;}
    case 11 : { console.log(hari + " November " + tahun); break;}
    case 12 : { console.log(hari + " Desember " + tahun);}
}
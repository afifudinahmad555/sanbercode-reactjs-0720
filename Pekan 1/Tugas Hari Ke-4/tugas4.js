// No. 1
console.log("==========Soal No.1==========")

let loop1 = 2;
let loop2 = 20;

console.log("LOOPING PERTAMA");
while (loop1 <= 20) {
    console.log(`${loop1} - I love coding`);
    loop1 += 2;
}

console.log("LOOPING KEDUA");
while (loop2 >= 2) {
    console.log(`${loop2} - I will become a frontend developer`);
    loop2 -= 2;
}

// No. 2
console.log("==========Soal No.2==========")

for (let nourut = 1; nourut <= 20; nourut++) {
    if (nourut % 3 === 0 && nourut % 2 === 1) {
        console.log(`${nourut} - I Love Coding`)
    } else if (nourut % 2 === 0) {
        console.log(`${nourut} - Berkualitas`)
    } else {
        console.log(`${nourut} - Santai`)
    }    
}

// No. 3
console.log("==========Soal No.3==========")

pagar = "";
for (let baris = 1; baris <= 7; baris++) {
    pagar += "#";
    console.log(pagar);
}

// No. 4
console.log("==========Soal No.4==========")

var kalimat = "saya sangat senang belajar javascript";
var pisah  = kalimat.split(" ");

console.log(pisah);

// No. 5
console.log("==========Soal No.5==========")
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortBuah = daftarBuah.sort();

for (let i = 0; i < sortBuah.length; i++) {
    console.log(sortBuah[i]);
}
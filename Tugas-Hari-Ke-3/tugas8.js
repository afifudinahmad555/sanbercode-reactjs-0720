// No 1.
console.log("==========Soal No.1==========")

const luasLingkaran = jari2 => {
    return Math.PI * (jari2^2)
}

let jari2Lingkaran = 7;
console.log(luasLingkaran(jari2Lingkaran));

// No 2.
console.log("==========Soal No.2==========")

let kalimat = "";

const fungsiPenambahanKata = (a,b,c,d,e) => {
    kalimat += `${a} ${b} ${c} ${d} ${e}`
    return kalimat;
}

let kalimatBaru = fungsiPenambahanKata("saya", "adalah", "seorang", "frontend", "developer")
console.log(kalimatBaru);

// No 3.
console.log("==========Soal No.3==========")

class Book {
    constructor(name, totalPage, price) {
        this._name = name;
        this._totalPage = totalPage;
        this._price = price;
    }

    get bookName () {
        return this._name 
    }

    set bookName (x) {
        this._name = x;
    }
    
    get bookPage () {
        return this._totalPage 
    }

    set bookPage (x) {
        this._totalPage = x;
    }

    get bookPrice () {
        return this._price 
    }

    set bookPrice (x) {
        this._price = x;
    }
}

class Comic extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price);
        this._isColorful = isColorful;
    }

    get bookColorful () {
        return this._isColorful 
    }

    set bookColorful (x) {
        this._isColorful = x;
    }
}

let bobo = new Book("Bobo", 26, 18000);
let sudoku = new Book("Sudoku", 16, 10000)
let conan = new Comic("Detective Conan", 130, 20000, false);
console.log(bobo);
console.log(sudoku);
console.log(conan);